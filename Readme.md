---
ECF: JUIN 2022
Document: Manuel d'utilisateur ECF Groupe-hotelier
Auteur: Florent Malo
Nom de l'hotel :  Orbit
---
# configuer son projet en local sur sa machine.
Dans mon cas de configuration j'ai travaillé sur un environnement Windows en OS de base combiné avec un Wsl2.
Assurez vous d'avoir installer un certains nombre d'outils  :
* Xampp
* PHP dans sa dernière  version (contenue dans Xampp)
* Composer pour la gestion des bibliothèques, et des dépendances sous symfony.
* Avoir un compte github,  ou gitlab.
* symfony CLI
* Docker
* Docker-compose
Pour vérifier les pré-requis (Sauf Docker et Docker-compose) lancer la commande suivante de symfony CLI
```
symfony check:requirements
```

## Initialiser le projet
* __Si d'aventure votre compte git n'est pas configuré les commandes utiles  :
git config --global user.email "you@example.com"
git config --global user.name "Your Name"___

Depuis la console, se rendre dans le dossier qui hébergera votre projet taper la commande suivante : 
```powershell
symfony new  "nom du projet " --webapp
```
___Dans mon exemple : symfony new GHSTUDI --webapp___ 

cd  votre repertoire projet.
code . repertoire
Ajouter un fichier readme, placer à la racine du projet. il sera alimenté pour la maintenance du projet au fur et à mésure.

## Lancer l'environnement de développement
```powershell
docker-compose up -d
symfony serve -d
```
