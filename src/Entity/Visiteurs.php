<?php

namespace App\Entity;

use App\Repository\VisiteursRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VisiteursRepository::class)]
class Visiteurs
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name_client;

    #[ORM\Column(type: 'string', length: 255)]
    private $firstname_client;

    #[ORM\Column(type: 'string', length: 255)]
    private $email;

    #[ORM\Column(type: 'string', length: 255)]
    private $passwd_client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameClient(): ?string
    {
        return $this->name_client;
    }

    public function setNameClient(string $name_client): self
    {
        $this->name_client = $name_client;

        return $this;
    }

    public function getFirstnameClient(): ?string
    {
        return $this->firstname_client;
    }

    public function setFirstnameClient(string $firstname_client): self
    {
        $this->firstname_client = $firstname_client;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPasswdClient(): ?string
    {
        return $this->passwd_client;
    }

    public function setPasswdClient(string $passwd_client): self
    {
        $this->passwd_client = $passwd_client;

        return $this;
    }
}
